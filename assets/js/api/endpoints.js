const endpoints = {
    baseUrl: {
        staging: 'https://stg.backend.pandemia.ecloudsolutions.com',
        production: 'https://api.cobeat.app'
    },
    user: {
        base: '/user',
        me: '/me',
        questionnaire: '/questionnaire'
    }
};