var headers;
var symptoms = 0;
var baseUrl = endpoints.baseUrl.production;

$(document).ready(function() {
    // Get params
    const params = getAllUrlParams();
    headers = {
        'X-Api-Key': params['x-api-key'],
        'Accept-Language': params['accept-language'],
        'Content-Type': 'application/json'
    }
    if (!isNullOrEmptyOrUndefined(endpoints.baseUrl[params['environment']])) {
        baseUrl = endpoints.baseUrl[params['environment']];
    }
    // Set carousel
    $('.carousel-form').carousel({
        fullWidth: true,
        indicators: true,
        touch: false,
        numVisible: 1,
        dist: 0,
        duration: 200,
        noWrap: true
    });
    removeCarouselTouchEvents();
    // Datepickers
    $('.datepicker').datepicker({
        autoClose: true,
        container: 'body',
        format: 'dd/mm/yyyy',
        i18n: {
            months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            weekdaysAbbrev: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
            cancel: 'Cancelar',
            close: 'Ok'
        }
    });
    // Set App Title
    setTitle('Cuestionario');
    // Check form by symptoms
    checkFormBySymtoms();
    // Get current user
    getCurrentUser();
});

function getCurrentUser() {
    showLoader();
    $.ajax({
        type: 'GET',
        headers: headers,
        url: baseUrl + endpoints.user.base + endpoints.user.me,
        dataType: 'json',
        success: function(response) {
            $('#firstName').val(response.data.firstName);
            $('#lastName').val(response.data.lastName);
            /********** Birthdate
            if (!isNullOrEmptyOrUndefined(response.data.birthdate)) {
                $('#birthdate').val(response.data.birthdate);
            }
            ***********/
            $('input[name=gender][value=' + response.data.gender + ']').prop('checked', true);
            $('input[name=pregnant][value=' + response.data.pregnant.toString() + ']').prop('checked', true);
            M.updateTextFields();
            hideLoader();
        }
    });
}

function updateCurrentUser() {
    const user = {
        firstName: $('#firstName').val(),
        lastName: $('#lastName').val(),
        gender: $('input[name=gender]:checked').val(),
        pregnant: $('input[name=pregnant]:checked').val() === 'true' ? true : false
    };
    $.ajax({
        type: 'PUT',
        headers: headers,
        url: baseUrl + endpoints.user.base + endpoints.user.me,
        data: JSON.stringify(user),
        dataType: 'json'
    });
}

function sendQuestionnaire() {
    showLoader();
    const questionnaire = {
        "questionnaire": []
    }
    // Load A, B, C items
    const items = [
        { letter: 'A', quantity: 11 },
        { letter: 'B', quantity: 3 },
        { letter: 'C', quantity: 7 }
    ]
    items.forEach( item => {
        for(let i = 1; i <= item.quantity; i++) {
            questionnaire['questionnaire'].push({
                "answer": getAnswerForYesNoCheckbox(item.letter + '.' + i),
                "question": item.letter + "." + i
            });
        }
    });
    // If the colaborator don't have any symptoms, send empty answers
    questionnaire['questionnaire'].push(...[
        {
            "answer": symptoms ? $('input[name="C.A.1"]:checked').val() : '',
            "question": "C.A.1"
        },
        {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.A"]').val() : '',
            "question": "C.A.1.A"
        },
        {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.B"]').val() : '',
            "question": "C.A.1.B"
        },
        {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.C"]').val() : '',
            "question": "C.A.1.C"
        },
        {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.Trip"]:checked').val() === 'Avión' ? 'Si' : 'No' : '',
            "question": "C.A.1.D"
        },
            {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.Trip"]:checked').val() === 'Barco' ? 'Si' : 'No' : '',
            "question": "C.A.1.E"
        },
            {
            "answer": symptoms && $('input[name="C.A.1"]:checked').val() === 'Si' ? $('input[name="C.A.1.Trip"]:checked').val() === 'Ómnibus' ? 'Si' : 'No' : '',
            "question": "C.A.1.F"
        },
            {
            "answer": symptoms ? $('input[name="C.A.1.G"]').val() : '',
            "question": "C.A.1.G"
        },
        {
            "answer": symptoms ? $('input[name="C.A.1.H"]').val() : '',
            "question": "C.A.1.H"
        },
        {
            "answer": symptoms ? $('input[name="C.A.2"]:checked').val() : '',
            "question": "C.A.2"
        },
        {
            "answer": symptoms && $('input[name="C.A.2"]:checked').val() === 'Si' ? $('input[name="C.A.2.A"]').val() : '',
            "question": "C.A.2.A"
        },
        {
            "answer": symptoms && $('input[name="C.A.2"]:checked').val() === 'Si' ? $('input[name="C.A.2.B"]').val() : '',
            "question": "C.A.2.B"
        },
        {
            "answer": symptoms && $('input[name="C.A.2"]:checked').val() === 'Si' ? $('input[name="C.A.2.C"]').val() : '',
            "question": "C.A.2.C"
        },
        {
            "answer": symptoms ? $('input[name="C.A.3"]:checked').val() : '',
            "question": "C.A.3"
        },
        {
            "answer": symptoms && $('input[name="C.A.3"]:checked').val() === 'Si' ? $('input[name="C.A.3.A"]').val() : '',
            "question": "C.A.3.A"
        },
        {
            "answer": symptoms ? $('input[name="C.A.4"]:checked').val() : '',
            "question": "C.A.4"
        },
        {
            "answer": symptoms && $('input[name="C.A.4"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.4.A') : '',
            "question": "C.A.4.A"
        },
        {
            "answer": symptoms && $('input[name="C.A.4"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.4.B') : '',
            "question": "C.A.4.B"
        },
        {
            "answer": symptoms && $('input[name="C.A.4"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.4.C') : '',
            "question": "C.A.4.C"
        },
        {
            "answer": symptoms && $('input[name="C.A.4"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.4.D') : '',
            "question": "C.A.4.D"
        },
        {
            "answer": symptoms && $('input[name="C.A.4"]:checked').val() === 'Si' ? $('input[name="C.A.4.E"]').val() : '',
            "question": "C.A.4.E"
        },
        {
            "answer": symptoms ? $('input[name="C.A.5"]:checked').val() : '',
            "question": "C.A.5"
        },
        {
            "answer": symptoms && $('input[name="C.A.5"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.5.A') : '',
            "question": "C.A.5.A"
        },
        {
            "answer": symptoms && $('input[name="C.A.5"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.5.B') : '',
            "question": "C.A.5.B"
        },
        {
            "answer": symptoms && $('input[name="C.A.5"]:checked').val() === 'Si' ? getAnswerForYesNoCheckbox('C.A.5.C') : '',
            "question": "C.A.5.C"
        },	
        {
            "answer": symptoms ? $('input[name="C.A.6"]:checked').val() : '',
            "question": "C.A.6"
        },
        {
            "answer": symptoms && $('input[name="C.A.6"]:checked').val() === 'Si' ? $('input[name="C.A.6.A"]').val() : '',
            "question": "C.A.6.A"
        },
            {
            "answer": symptoms && $('input[name="C.A.6"]:checked').val() === 'Si' ? $('input[name="C.A.6.B"]').val() : '',
            "question": "C.A.6.B"
        },
            {
            "answer": symptoms && $('input[name="C.A.6"]:checked').val() === 'Si' ? $('input[name="C.A.6.C"]').val() : '',
            "question": "C.A.6.C"
        },
        {
            "answer": symptoms ? $('input[name="D.1"]').val() : '',
            "question": "D.1"
        },
        {
            "answer": symptoms ? $('input[name="D.2"]').val() : '',
            "question": "D.2"
        },
        {
            "answer": symptoms ? $('input[name="D.3"]').val() : '',
            "question": "D.3"
        },
        {
            "answer": symptoms ? $('input[name="D.4"]').val() : '',
            "question": "D.4"
        },
        {
            "answer": symptoms ? $('input[name="D.5"]').val() : '',
            "question": "D.5"
        },
        {
            "answer": symptoms ? $('input[name="D.6"]').val() : '',
            "question": "D.6"
        }
    ]);
    $.ajax({
        type: 'PUT',
        headers: headers,
        url: baseUrl + endpoints.user.base + endpoints.user.questionnaire,
        data: JSON.stringify(questionnaire),
        dataType: 'json',
        success: function(response) {
            setTitle('Cálculo de probabilidad');
            $('#questionnaire').addClass('hide');
            $('#results').removeClass('hide');
            hideLoader();
        }
    });
}

function getAnswerForYesNoCheckbox(name) {
    return $('input[type="checkbox"][name="' + name + '"]').is(':checked') ? 'Si' : 'No';
}

function isChecked(name) {
    return $('input[type="checkbox"][name="' + name + '"]').is(':checked');
}

function showLoader() {
    $('#loader').removeClass('hide');
}

function hideLoader() {
    $('#loader').addClass('hide');
}

function isEmpty(element) {
    return element === '';
}

function isNullOrEmptyOrUndefined(element) {
    let valid = false;
    if (element === '' || element === null || element === undefined) {
        valid = true;
    }
    return valid;
}

function convertDateToApi(date) {
    const splitDate = date.split('/');
    return splitDate[2] + '-' + splitDate[1] + '-' + splitDate[0];
}

function convertDateFromApi(date) {
    const splitDate = date.split('-');
    return splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0];
}

function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  
    // we'll store the parameters here
    var obj = {};
  
    // if query string exists
    if (queryString) {
  
      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];
  
      // split our query string into its component parts
      var arr = queryString.split('&');
  
      for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');
  
        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
  
        // (optional) keep case consistent
        paramName = paramName.toLowerCase();
        if (typeof paramValue === 'string') paramValue = paramValue;
  
        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {
  
          // create key if it doesn't exist
          var key = paramName.replace(/\[(\d+)?\]/, '');
          if (!obj[key]) obj[key] = [];
  
          // if it's an indexed array e.g. colors[2]
          if (paramName.match(/\[\d+\]$/)) {
            // get the index value and add the entry at the appropriate position
            var index = /\[(\d+)\]/.exec(paramName)[1];
            obj[key][index] = paramValue;
          } else {
            // otherwise add the value to the end of the array
            obj[key].push(paramValue);
          }
        } else {
          // we're dealing with a string
          if (!obj[paramName]) {
            // if it doesn't exist, create property
            obj[paramName] = paramValue;
          } else if (obj[paramName] && typeof obj[paramName] === 'string'){
            // if property does exist and it's a string, convert it to an array
            obj[paramName] = [obj[paramName]];
            obj[paramName].push(paramValue);
          } else {
            // otherwise add the property
            obj[paramName].push(paramValue);
          }
        }
      }
    }
  
    return obj;
}

function setTitle(title) {
    if(typeof Mobile !== 'undefined'){
        Mobile.setTitle(title);
    }
}

function goToHome() {
    if(typeof Mobile !== 'undefined'){
        Mobile.goToHome();
    }
}

function checkFormBySymtoms() {
    showLoader();
    if (symptoms) {
        $('.symptoms button[type="submit"]').addClass('hide');
        $('.symptoms button[type="button"]').removeClass('hide');
        $('.trips-exposed-risk,.persons-in-contact').removeClass('hide');
        $('.carousel-form .indicators .indicator-item:nth-child(5),.carousel-form .indicators .indicator-item:nth-child(6)').removeClass('hide');
    } else {
        $('.symptoms button[type="submit"]').removeClass('hide');
        $('.symptoms button[type="button"]').addClass('hide');
        $('.trips-exposed-risk,.persons-in-contact').addClass('hide');
        $('.carousel-form .indicators .indicator-item:nth-child(5),.carousel-form .indicators .indicator-item:nth-child(6)').addClass('hide');
    }
    hideLoader();
}

function removeCarouselTouchEvents() {

}

$(document).on('submit', '#questionnaire', function() {
    updateCurrentUser();
    sendQuestionnaire();
    return false;
});

$(document).on('click', '.next-slide', function() {
    $('.carousel-form').carousel('next');
});

$(document).on('click', '.go-back', function() {
    goToHome();
});

$(document).on('click', '.show-element-by-id', function(){
    const idElement = $(this).attr('data-element-id');
    $('#' + idElement).removeClass('hide');
});

$(document).on('click', '.hide-element-by-id', function(){
    const idElement = $(this).attr('data-element-id');
    $('#' + idElement).addClass('hide');
});

$(document).on('click', '.symptoms input[type=checkbox]', function() {
    $(this).is(':checked') ? symptoms++ : symptoms--;
    checkFormBySymtoms();
});